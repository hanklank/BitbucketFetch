#!/bin/bash -ex

#Simple script used to clone and pull all bitbucket repos for a specific project key (common setup with one repo owner with many teams/project groups
#In parallell so pretty fast.

#Note1: uses git ssh repos so that you wont have to input usr/pwd (as you would have with https-path)
#Note2:Only meant to be run from the root of your projectdir. 

BB_USER=$1
BB_PWD=$2
BB_PROJ_KEY=$3
BB_REPO_OWNER=$4
BB_REPOS_PREFIX=$5

check_input () 
{
  if [ "$1" -ne 5 ]
  then
   echo "Wrong nr of arguments supplied - should be: user password projectkey repoowner repoprefix"
   exit 1
  fi
}

check_input "$#"

curl https://api.bitbucket.org/2.0/repositories/"$BB_REPO_OWNER"/?q\='project.key="'"$BB_PROJ_KEY"'"'\&pagelen=100 -u $BB_USER:$BB_PWD \
 | jq '.values[].links.clone[1].href' \
 | sort \
 | grep -v "obsolete" \
 | tr -d '"' \
 | xargs -n 1 -P 8 git clone


find . -maxdepth 1 -type d \( ! -name . \) -name "$BB_REPOS_PREFIX" -print0 | xargs -P 8 -0 -L1 sh -c 'cd "$0" && git pull'

